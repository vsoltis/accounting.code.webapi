using System;
using System.Threading.Tasks;
using accounting.code.webapi.Models;
using accounting.code.webapi.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace accounting.code.webapi.Controllers.Api
{
	[Route("api/category")]
	public class CategoryController : Controller
	{
		private ILogger<CategoryController> _logger;
		private IAccountingRepository _repository;

		public CategoryController(IAccountingRepository repository, ILogger<CategoryController> logger)
		{
			_repository = repository;
			_logger = logger;
		}


		[HttpGet("")]
		public IActionResult Get()
		{
			try
			{
				var result = _repository.GetAllCategories();

				return Ok(result);
			}
			catch (Exception ex)
			{
				_logger.LogError($"Failed to get All Categories: {ex}");

				return BadRequest("Error occurred");
			}
		}

		[HttpGet("{categoryId}")]
		public IActionResult Get(int categoryId)
		{
			try
			{
				var result = _repository.GetCategoryById(categoryId);

				return Ok(result);
			}
			catch (Exception ex)
			{
				_logger.LogError($"Failed to get Category by ID '{categoryId}': {ex}");

				return BadRequest("Error occurred");
			}
		}

		[HttpPost("")]
		public async Task<IActionResult> Post(CategoryViewModel category)
		{
			try
			{
				if (ModelState.IsValid)
				{
					// Save to the Database
					var newCategory = Mapper.Map<Category>(category);

					newCategory.DateCreated = DateTime.UtcNow;

					_repository.AddCategory(newCategory);

					if (await _repository.SaveChangesAsync())
					{
						return Created($"api/category/{category.Name}", Mapper.Map<CategoryViewModel>(category));
					}

				}

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError($"Failed to create Category '{category.Name}': {ex}");

				return BadRequest("Failed to create category");
			}
		}

		[HttpDelete("{categoryId}")]
		public async Task<IActionResult> Delete(int categoryId)
		{
			try
			{
				var category = _repository.GetCategoryById(categoryId);

				_repository.DeleteCategory(category);

				if (await _repository.SaveChangesAsync())
				{
					return Ok(categoryId);
				}

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError($"Failed to delete Category by ID '{categoryId}': {ex}");

				return BadRequest("Failed to delete category");
			}
		}
	}
}
