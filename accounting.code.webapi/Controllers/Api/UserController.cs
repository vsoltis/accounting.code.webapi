using System;
using accounting.code.webapi.Models;
using accounting.code.webapi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace accounting.code.webapi.Controllers.Api
{
	[Route("/api/user")]
	public class UserController : Controller
	{
		private GeoCoordsService _coordsService;
		private ILogger<UserController> _logger;
		private IAccountingRepository _repository;

		public UserController(IAccountingRepository repository,
		  ILogger<UserController> logger,
		  GeoCoordsService coordsService)
		{
			_repository = repository;
			_logger = logger;
			_coordsService = coordsService;
		}

		[HttpGet("{userId}")]
		public IActionResult Get(string userId)
		{
			try
			{
				var user = _repository.GetUserById(userId);

				return Ok(user);
			}
			catch (Exception ex)
			{
				_logger.LogError("Failed to get user by ID '{0}': {1}", ex, userId);
			}

			return BadRequest("Failed to get stops");
		}
	}
}
