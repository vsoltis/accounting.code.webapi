using accounting.code.webapi.Models;
using accounting.code.webapi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace accounting.code.webapi.Controllers.Web
{
	public class AppController : Controller
	{
		private IMailService _mailService;
		private IConfigurationRoot _config;
		private IAccountingRepository _repository;
		private ILogger<AppController> _logger;

		public AppController(IMailService mailService,
		  IConfigurationRoot config,
		  IAccountingRepository repository,
		  ILogger<AppController> logger)
		{
			_mailService = mailService;
			_config = config;
			_repository = repository;
			_logger = logger;
		}

		public IActionResult Index()
		{
			return Ok("{'app.name': 'accounting.code.webapi', 'app.status': 'started'}");
		}
	}
}
