using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace accounting.code.webapi.Models
{
	public class AccountingContext : IdentityDbContext<AccountingUser>
	{
		private IConfigurationRoot _config;

		public AccountingContext(IConfigurationRoot config, DbContextOptions options)
		  : base(options)
		{
			_config = config;
		}
		public DbSet<Category> Categories { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);

			optionsBuilder.UseSqlServer(_config["ConnectionStrings:AccountingContextConnection"]);
		}
	}
}
