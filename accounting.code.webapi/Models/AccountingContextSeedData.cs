using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace accounting.code.webapi.Models
{
	public class AccountingContextSeedData
	{
		private AccountingContext _context;
		private UserManager<AccountingUser> _userManager;

		public AccountingContextSeedData(AccountingContext context, UserManager<AccountingUser> userManager)
		{
			_context = context;
			_userManager = userManager;
		}

		public async Task EnsureSeedData()
		{
			if (await _userManager.FindByEmailAsync("viktor.soltis@gmail.com") == null)
			{
				var user = new AccountingUser()
				{
					UserName = "viktor.soltis",
					Email = "viktor.soltis@gmail.com"
				};

				var result = await _userManager.CreateAsync(user, "Rolz1234@");
			}
		}
	}
}
