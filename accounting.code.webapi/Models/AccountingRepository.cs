using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace accounting.code.webapi.Models
{
	public class AccountingRepository : IAccountingRepository
	{
		private AccountingContext _context;
		private ILogger<AccountingRepository> _logger;

		public AccountingRepository(AccountingContext context, ILogger<AccountingRepository> logger)
		{
			_context = context;
			_logger = logger;
		}

		public AccountingUser GetUserById(string userId)
		{
			return _context.Users.FirstOrDefault(t => t.Id == userId);
		}

		public IEnumerable<Category> GetAllCategories()
		{
			return _context.Categories.ToList();
		}

		public Category GetCategoryById(int categoryId)
		{
			return _context.Categories.FirstOrDefault(t => t.Id == categoryId);
		}

		public void AddCategory(Category newCategory)
		{
			_context.Add(newCategory);
		}

		public void DeleteCategory(Category category)
		{
			_context.Remove(category);
		}

		public async Task<bool> SaveChangesAsync()
		{
			return (await _context.SaveChangesAsync()) > 0;
		}
	}
}
