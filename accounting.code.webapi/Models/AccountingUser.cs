using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace accounting.code.webapi.Models
{
	public class AccountingUser : IdentityUser
	{
		public DateTime FirstTrip { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
}
