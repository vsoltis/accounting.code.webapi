﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace accounting.code.webapi.Models
{
	public class Category
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int? CategoryId { get; set; }
		public DateTime DateCreated { get; set; }

		[ForeignKey("CategoryId")]
		public Category ParentCategory { get; set; }
	}
}