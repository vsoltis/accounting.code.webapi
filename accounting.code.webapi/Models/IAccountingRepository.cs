﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace accounting.code.webapi.Models
{
	public interface IAccountingRepository
	{
		AccountingUser GetUserById(string userId);
		Task<bool> SaveChangesAsync();
		IEnumerable<Category> GetAllCategories();
		Category GetCategoryById(int categoryId);

		void AddCategory(Category newCategory);
		void DeleteCategory(Category categoryId);
	}
}