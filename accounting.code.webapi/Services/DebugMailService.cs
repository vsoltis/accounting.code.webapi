using System.Diagnostics;

namespace accounting.code.webapi.Services
{
	public class DebugMailService : IMailService
	{
		public void SendMail(string to, string from, string subject, string body)
		{
			Debug.WriteLine($"Sending Mail: To: {to} From: {from} Subject: {subject}");
		}
	}
}
