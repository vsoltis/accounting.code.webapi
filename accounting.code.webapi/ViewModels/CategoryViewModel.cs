using System.ComponentModel.DataAnnotations;

namespace accounting.code.webapi.ViewModels
{
	public class CategoryViewModel
	{
		[Required]
		public string Name { get; set; }
		
		public int? CategoryId { get; set; }
	}
}
