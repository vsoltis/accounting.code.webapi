using System.ComponentModel.DataAnnotations;

namespace accounting.code.webapi.ViewModels
{
	public class LoginViewModel
	{
		[Required]
		public string UserName { get; set; }
		[Required]
		public string Password { get; set; }

	}
}
