using System.ComponentModel.DataAnnotations;

namespace accounting.code.webapi.ViewModels
{
	public class UserViewModel
	{
		[Required]
		[StringLength(100, MinimumLength = 5)]
		public string Name { get; set; }
		public string Email { get; set; }
	}
}
